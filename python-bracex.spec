# Created by pyp2rpm-3.3.5
%global pypi_name bracex

Name:           python-%{pypi_name}
Version:        2.5
Release:        1
Summary:        Bash style brace expander

License:        MIT
URL:            https://github.com/facelessuser/bracex
Source0:        https://github.com/facelessuser/bracex/archive/%{version}.tar.gz#/%{pypi_name}-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  python3-devel
BuildRequires:  python3dist(pytest)
BuildRequires:  python3-setuptools
BuildRequires:  python3-setuptools_scm
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
BuildRequires:  python3-hatchling

%description
Bracex is a brace expanding library (à la Bash) for Python.
Brace expanding is used to generate arbitrary strings.

%package -n     python3-%{pypi_name}
Summary:        %{summary}

%description -n python3-%{pypi_name}
Bracex is a brace expanding library (à la Bash) for Python.
Brace expanding is used to generate arbitrary strings.


%prep
%autosetup -n %{pypi_name}-%{version}
# Remove bundled egg-info
rm -rf %{pypi_name}.egg-info

%build
%pyproject_build

%install
%pyproject_install

%files -n python3-%{pypi_name}
%license LICENSE.md docs/src/markdown/about/license.md
%doc README.md
%{python3_sitelib}/%{pypi_name}
%{python3_sitelib}/bracex-*.dist-info/

%changelog
* Mon Aug 19 2024 zhangxianting <zhangxianting@uniontech.com> - 2.5-1
- Update package to version 2.5
- New Add Python 3.13 support

* Mon Nov 20 2023 jiangxinyu <jiangxinyu@kylinos.cn> - 2.4-1
- Update package to version 2.4

* Wed Jun 21 2023 li-miaomiao_zhr <mmlidc@isoftstone.com> - 2.3-1
- add package of python-bracex with version 2.3 to src-openEuler
